"use client";
import React from "react";
import { useTheme } from "next-themes";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { logout } from "../redux/auth/auth";
import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  NavbarMenu,
  NavbarMenuItem,
  NavbarMenuToggle,
  Link,
  Button,
} from "@nextui-org/react";
import Image from "next/image";
import { useRouter } from "next/navigation";
import SunIcon from "./SunIcon";
import MoonIcon from "./MoonIcon";

export default function NavbarSect() {
  const [mounted, setMounted] = useState(false);
  const { theme, setTheme } = useTheme();
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  const isLogin = useSelector((state) => state.auth.isLogin);
  const dispatch = useDispatch();
  const router = useRouter();

  const logoutHandler = () => {
    dispatch(logout());
    router.push("/");
  };

  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) return null;

  const menuItems = ["Sign In", "Sign Up"];

  return (
    <Navbar onMenuOpenChange={setIsMenuOpen} position="static" maxWidth="2xl">
      <NavbarContent>
        <NavbarMenuToggle
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
          className="sm:hidden"
        />
        <NavbarBrand>
          <Link href="/">
            <Image
              src="/images/logo.png"
              width={24}
              height={24}
              alt="logo.png"
            />
          </Link>
        </NavbarBrand>
      </NavbarContent>
      {isLogin === true ? (
        <NavbarContent className="hidden sm:flex gap-4" justify="center">
          <h3>Welcome!</h3>
          <NavbarItem>
            <Link href="/home">Homepage</Link>
          </NavbarItem>
          <NavbarItem>
            <Link href="/game-list" aria-current="page">
              Game List
            </Link>
          </NavbarItem>
          <NavbarItem>
            <Link href="#">User List</Link>
          </NavbarItem>
        </NavbarContent>
      ) : null}
      <NavbarContent justify="end">
        {isLogin === false ? (
          <>
            <NavbarItem>
              <Button
                as={Link}
                color="primary"
                href="/register"
                variant="ghost"
              >
                Sign Up
              </Button>
            </NavbarItem>
            <NavbarItem>
              <Button as={Link} color="primary" href="/login" variant="shadow">
                Sign In
              </Button>
            </NavbarItem>
          </>
        ) : (
          <NavbarItem>
            <Button color="primary" variant="ghost" onClick={logoutHandler}>
              Logout
            </Button>
          </NavbarItem>
        )}
        <NavbarItem>
          <Button
            isIconOnly
            variant="primary"
            onClick={() => setTheme("light")}
          >
            <SunIcon />
          </Button>
        </NavbarItem>
        <NavbarItem>
          <Button isIconOnly variant="outline" onClick={() => setTheme("dark")}>
            <MoonIcon />
          </Button>
        </NavbarItem>
      </NavbarContent>
      <NavbarMenu>
        {menuItems.map((item, index) => (
          <NavbarMenuItem key={`${item}-${index}`}>
            <Link
              color={
                index === 2
                  ? "primary"
                  : index === menuItems.length - 1
                  ? "danger"
                  : "foreground"
              }
              className="w-full"
              href="#"
              size="lg"
            >
              {item}
            </Link>
          </NavbarMenuItem>
        ))}
      </NavbarMenu>
    </Navbar>
  );
}
