"use client";
import React from "react";
import { Image } from "@nextui-org/react";

export default function PlayerChoice({
  choices,
  playGameFunc,
  setPlayerChoice,
}) {
  return (
    <div className="col-4 d-flex flex-column align-items-center player-group">
      <h2 className="text-2xl">PLAYER</h2>
      <div className="player-button-group">
        {choices.map((choice, index) => (
          <div className={`${choice}-button-group`} key={index}>
            <button
              type="submit"
              className={`btn ${choice}-p w-[125px] h-[175px]`}
              key={choice}
              value={choice}
              onClick={() => playGameFunc(choice)}
              onChange={(e) => setPlayerChoice(e.target.value)}
            >
              <Image
                src={`/images/${choice}.png`}
                alt={`${choice}.png`}
                className={`${choice}-image w-full h-full`}
              />
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}
