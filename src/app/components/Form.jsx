"use client";
import React from "react";
import { Input } from "@nextui-org/react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/navigation";
import { login } from "../redux/auth/auth";

export default function Form({
  title,
  buttonText,
  signText,
  signLink,
  signHref,
}) {
  const dispatch = useDispatch();
  const router = useRouter();

  const loginHandler = (e) => {
    e.preventDefault();

    dispatch(login());
    router.push("/home");
  };
  return (
    <div className="w-full bg-white rounded-lg shadow dark:shadow md:mt-0 sm:max-w-md xl:p-0 dark:bg-black dark:shadow-gray-950">
      <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
          {title}
        </h1>
        <form
          className="space-y-4 md:space-y-6"
          onSubmit={title === "Sign In" && loginHandler}
        >
          {title === "Sign Up" && (
            <Input
              type="username"
              label="Username"
              name="username"
              id="username"
              labelPlacement="outside"
              className="bg-gray-50  text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-transparent dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="name@company.com"
              isRequired
            />
          )}
          <Input
            type="email"
            label="Email Address"
            name="email"
            id="email"
            labelPlacement="outside"
            className="bg-gray-50  text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-transparent dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="name@company.com"
            isRequired
          />
          <Input
            type="password"
            label="Password"
            name="password"
            id="password"
            labelPlacement="outside"
            className="bg-gray-50  text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-transparent dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            placeholder="your password"
            isRequired
          />
          {title === "Sign In" && (
            <div className="flex items-center justify-between">
              <a
                href="#"
                className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"
              >
                Forgot password?
              </a>
            </div>
          )}

          <button
            type="submit"
            className="w-full text-white bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800"
          >
            {buttonText}
          </button>
          <p className="text-sm font-light text-gray-500 dark:text-gray-400">
            {signText}
            <a
              href={signHref}
              className="font-medium text-primary-600 hover:underline dark:text-primary-500"
            >
              {signLink}
            </a>
          </p>
        </form>
      </div>
    </div>
  );
}
