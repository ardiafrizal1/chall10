"use client";
import React from "react";
import { Image } from "@nextui-org/react";

export default function ComputerChoice({ choices, computerChoice }) {
  return (
    <div className="col-4 d-flex flex-column align-items-center computer-group">
      <h2 className="text-2xl">COMPUTER</h2>
      <div className="computer-button-group">
        {choices.map((choice) => (
          <div className={`${choice}-button-group`} key={choice}>
            <button
              className={`btn ${choice}-p w-[125px] h-[175px]`}
              key={choice}
              style={computerChoice === choice ? { opacity: "100%" } : {}}
              disabled
            >
              <Image
                src={`/images/${choice}.png`}
                alt={`${choice}.png`}
                className={`${choice}-image`}
              />
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}
