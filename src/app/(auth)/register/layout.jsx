import NavbarSect from "@/app/components/Navbar";
import React from "react";

export default function RegisterLayout({ children }) {
  return (
    <>
      <NavbarSect />
      <main>
        <div className="flex items-center my-32">
          <div className="w-full">
            <div className="flex justify-center">{children}</div>
          </div>
        </div>
      </main>
    </>
  );
}
