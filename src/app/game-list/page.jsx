"use client";
import React from "react";
import {
   Card,
   CardHeader,
   CardBody,
   CardFooter,
   Image,
   Button,
   Link,
} from "@nextui-org/react";

export default function Page() {
   return (
      <div className="flex justify-center mt-12 max-[640px]: flex-wrap">
         <div className="min-tablet: mt-2">
            <Card
               isFooterBlurred
               className="w-full h-[300px] col-span-12 sm:col-span-5"
            >
               <CardHeader className="absolute z-10 top-1 flex-col items-start">
               </CardHeader>
               <Image
                  removeWrapper
                  alt="Card example background"
                  className="z-0 w-full h-full scale-125 -translate-y-6 object-cover"
                  src="/images/roullete.avif"
               />
               <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">
                  <div>
                     <p className="text-black text-tiny">Available soon.</p>
                     <p className="text-black text-tiny">Get notified.</p>
                  </div>
                  <Button
                     className="text-tiny"
                     color="primary"
                     radius="full"
                     size="sm"
                  >
                     Notify Me
                  </Button>
               </CardFooter>
            </Card>
         </div>
         <div className="mx-4 min-tablet: mt-2">
            <Card
               isFooterBlurred
               className="w-full h-[300px] col-span-12 sm:col-span-5"
            >
               <CardHeader className="absolute z-10 top-1 flex-col items-start">
               </CardHeader>
               <Image
                  removeWrapper
                  alt="Card example background"
                  className="z-0 w-full h-full scale-125 -translate-y-6 object-cover"
                  src="/images/rps.avif"
               />
               <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">
                  <Button
                     className="text-tiny"
                     color="primary"
                     radius="full"
                     size="sm"
                     href="/game-play"
                     as={Link}
                  >
                     Play
                  </Button>
                  <Button
                     className="text-tiny"
                     color="primary"
                     radius="full"
                     size="sm"
                     href="/game-detail"
                     as={Link}
                  >
                     Detail Game
                  </Button>
               </CardFooter>
            </Card>
         </div>
         <div className="min-tablet: mt-2">
            <Card
               isFooterBlurred
               className="w-full h-[300px] col-span-12 sm:col-span-5"
            >
               <CardHeader className="absolute z-10 top-1 flex-col items-start">
               </CardHeader>
               <Image
                  removeWrapper
                  alt="Card example background"
                  className="z-0 w-full h-full scale-125 -translate-y-6 object-cover"
                  src="/images/vr.avif"
               />
               <CardFooter className="absolute bg-white/30 bottom-0 border-t-1 border-zinc-100/50 z-10 justify-between">
                  <div>
                     <p className="text-black text-tiny">Available soon.</p>
                     <p className="text-black text-tiny">Get notified.</p>
                  </div>
                  <Button
                     className="text-tiny"
                     color="primary"
                     radius="full"
                     size="sm"
                  >
                     Notify Me
                  </Button>
               </CardFooter>
            </Card>
         </div>
      </div>
   );
}
