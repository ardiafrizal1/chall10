"use client";
import React, { useEffect } from "react";
import "./Rps.css";
import { useSelector } from "react-redux";
import PlayerChoice from "../components/PlayerChoice";
import Result from "../components/Result";
import ComputerChoice from "../components/ComputerChoice";
import { useDispatch } from "react-redux";
import {
  setPlayerChoice,
  setComputerChoice,
  setLose,
  setWin,
  setDraw,
  resetGameState,
} from "../redux/gameSlice";

function Rps() {
  const state = useSelector((state) => state.game);
  const playerChoice = useSelector((state) => state.game.playerChoice);
  const computerChoice = useSelector((state) => state.game.computerChoice);
  const dispatch = useDispatch();

  const playGameFunc = (choice) => {
    dispatch(resetGameState());
    dispatch(setPlayerChoice(choice));
    dispatch(setComputerChoice());
  };

  useEffect(() => {
    if (playerChoice !== null && computerChoice !== null) {
      // Perform game logic using playerChoice and computerChoice
      if (playerChoice === computerChoice) {
        dispatch(setDraw());
      } else if (
        (playerChoice === "rock" && computerChoice === "scissor") ||
        (playerChoice === "paper" && computerChoice === "rock") ||
        (playerChoice === "scissor" && computerChoice === "paper")
      ) {
        dispatch(setWin());
      } else {
        dispatch(setLose());
      }
    }
  }, [playerChoice, computerChoice, dispatch]);

  return (
    <>
      <section className="body">
        <div className="container">
          <div className="flex justify-around ">
            <h1 className="text-4xl my-7">
              {" "}
              <span>Rock Paper Scissors</span>
            </h1>
          </div>
          <div>
            <div className="flex flex-col justify-around px-36 my-10">
              <h2 className="text-xl">Round Saat ini : {state.round}</h2>
              <h2 className="text-xl ">Score : {state.total_score}</h2>
            </div>
          </div>
          <div className="flex justify-between px-32">
            <PlayerChoice
              choices={state.choices}
              setPlayerChoice={setPlayerChoice}
              playGameFunc={playGameFunc}
            />
            <Result
              playerChoice={playerChoice}
              computerChoice={computerChoice}
              result={state.result}
            />
            <ComputerChoice
              choices={state.choices}
              computerChoice={state.computerChoice}
              setComputerChoice={setComputerChoice}
            />
          </div>
        </div>
      </section>
    </>
  );
}
export default Rps;
