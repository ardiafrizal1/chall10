'use client';

import { combineReducers, configureStore } from '@reduxjs/toolkit';
import authReducer from './auth/auth';
import gameReducer from './gameSlice/index';

const rootReducer = combineReducers({
  auth: authReducer,
  game: gameReducer,
  //add another reducer here
});

export const store = configureStore({
  reducer: rootReducer,
});
