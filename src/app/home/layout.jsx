import React from "react";
import NavbarSect from "../components/Navbar";

export default function HomeLayout({ children }) {
  return (
    <>
      <NavbarSect />
      <main className="flex justify-center">{children}</main>
    </>
  );
}
