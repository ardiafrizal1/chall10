import './globals.css';
import { Inter } from 'next/font/google';
import NextProvider from './nextProvider';
import { ReduxProvider } from './redux/provider';

const inter = Inter({ subsets: ['latin'] });

export const metadata = {
  title: 'Create Next App',
  description: 'Generated by create next app',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body className={`${inter.className} bg-gray-100 dark:bg-slate-900 h-full w-full`}>
        <ReduxProvider>
          <NextProvider>{children}</NextProvider>
        </ReduxProvider>
      </body>
    </html>
  );
}
